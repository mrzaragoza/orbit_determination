%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% rotation_matrix: function description
function matrix = rotation_matrix(axis_name, angle)
	if axis_name == 'x'
		matrix = rx(angle);
	elseif axis_name == 'y'
		matrix = ry(angle);
	elseif axis_name == 'z'
		matrix = rz(angle);
	else
		throw('axis_name not valid. It should be x, y or z');
	end	
end

function r_x = rx(angle)
    r_x =  [1   0             0; ...
            0   cosd(angle)   sind(angle); ...
            0   -sind(angle)  cosd(angle)];
end

function r_y = ry(angle)
    r_y =  [cosd(angle) 0   -sind(angle); ...
            0           1   0; ...
            sind(angle) 0   cosd(angle)];
end

function r_z = rz(angle)
    r_z =  [cosd(angle)     sind(angle) 0; ...
            -sind(angle)    cosd(angle) 0; ...
            0               0           1];
end