%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Space Master 2018, Round 14                       %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [t, st, st0] = orbit_simulation_3D(e, a, Me, right_asc_node, incl, arg_perigee, n_periods)

    % General constants
    data = Data;

    % Variables specific for the problem to solve
    global mu;
    mu              = data.G * data.Earth_mass;
    h               = sqrt(a * mu * (1 - e^2));
    period          = 2 * pi * a ^ (3/2) / sqrt(mu); % seconds
    
    % Obtains the initial true anomaly from e and Me
    if e > 0
        true_anom   = obtain_true_anomaly_elliptical_orbits(Me, e);
    elseif e == 0
        true_anom   = Me;
    end

    % Obtains the state vector for the initial true anomaly (position and
    % velocity)

    r0_magnitude    = h^2 / (mu * (1 + e * cosd(true_anom)));
    r0x             = r0_magnitude * cosd(true_anom);
    r0y             = r0_magnitude * sind(true_anom);
    r0z             = 0;

    rot_matrix = rotation_matrix('z', right_asc_node)'  * ...
                      rotation_matrix('x', incl)' * ... 
                      rotation_matrix('z', arg_perigee)';
    
    r0              = rot_matrix * [r0x; r0y; r0z];

    v0x             = - (mu / h) * (1 + e * cosd(true_anom)) * sind(true_anom) + ...
                    (mu / h) * e * sind(true_anom) * cosd(true_anom);
    v0y             = (mu / h) * (1 + e * cosd(true_anom)) * cosd(true_anom) + ...
                    (mu / h) * e * sind(true_anom) * sind(true_anom);
    v0z             = 0;

    v0              = rot_matrix * [v0x; v0y; v0z];

    st0             = [r0 v0];
    st0             = double(st0);

    % Does the integration of the derivative of state vector over time using Runge-Kutta algorithm
    tspan           = [0, n_periods * period];
    options         = odeset('RelTol', 1e-6);
    [t, st]         = ode45(@satellite_orbit, tspan, st0, options);    
end
