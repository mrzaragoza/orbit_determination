%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Space Master 2018, Round 14                       %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function true_anom = obtain_true_anomaly_elliptical_orbits(mean_anomaly, eccentricity)
	syms E_sym;
	E = vpasolve(E_sym == mean_anomaly + eccentricity * sind(E_sym), E_sym);
	syms true_anom_sym;
	true_anom = vpasolve(sind(E) == sqrt(1 - eccentricity^2) * sind (true_anom_sym) / ...
    	(1 + eccentricity * cosd(true_anom_sym)), true_anom_sym);
end