%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Space Master 2018, Round 14                       %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
clc
close

% General constants
data = Data;

% Choose the problem to solve [LEO, GTO, GEO, Molniya, Tundra, MEO]
orbit_chosen = data.LEO;

% Variables specific for the problem to solve
global mu;
e       = data.e(orbit_chosen);
a       = data.a(orbit_chosen);
Me      = data.Me(orbit_chosen);

rp      = a * (1 - e);
ra      = 2 * a - rp;
mu      = data.G * data.Earth_mass;
h       = sqrt(ra * mu * (1 - e));
period  = 2 * pi * a^(3/2) / sqrt(mu); % seconds

% Obtains the initial true anomaly from e and Me
true_anom = obtain_true_anomaly(Me, e);

% Obtains the state vector for the initial true anomaly (position and
% velocity)

r0  = h^2 / (mu * (1 + e * cosd(true_anom)));
r0x = r0 * cosd(true_anom);
r0y = r0 * sind(true_anom);
r0z = 0;

v0x = - (mu / h) * (1 + e * cosd(true_anom)) * sind(true_anom) + ...
        (mu / h) * e * sind(true_anom) * cosd(true_anom);
v0y = (mu / h) * (1 + e * cosd(true_anom)) * cosd(true_anom) + ...
        (mu / h) * e * sind(true_anom) * sind(true_anom);
v0z = 0;

st0 = [r0x r0y r0z v0x v0y v0z];
st0 = double(st0);

% Does the integration of the state vector using Runge-Kutta algorithm

tspan = [0, 10*period];
options = odeset('RelTol', 1e-6);
[t, st] = ode45(@satellite_orbit, tspan, st0, options);

% r = iteration_over_true_anomaly(true_anom, h, mu, e);

plot_satellite_data( st, st0, '2D', orbit_chosen)



