%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Space Master 2018, Round 14                       %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [right_ascension, declination] = ground_track_calculation(time, st)

	% General constants
    hoursday = 24 * 3600;
        
    r_pos = [st(:,1) st(:,2) st(:,3)];

    % define and preallocate variables
    right_ascension = zeros(length(st),1);
    declination = zeros(length(st),1);


    % Calculate position vector in respect to the earth's rotation
    % Calculate declination and right ascension from unit vector u_r

    for i = 1:1:length(time)
        % current rotation angle of the Earth
        deg = time(i) * 360 / hoursday; 
        
        % rotate the position vector of the satellite with the Earth's rotation
        r_pos_rotated = rotation_matrix('z', deg) * r_pos(i,:)'; 
        
        % calculate declination and right ascension
        r_magnitude = norm([st(i,1) st(i,2) st(i,3)]);
        unit_vector = r_pos_rotated / r_magnitude;
        declination(i) = asind(unit_vector(3));
        if (unit_vector(2) > 0)
            right_ascension(i) = acosd(unit_vector(1)/cosd(declination(i))) - 180;
        else
            right_ascension(i) = 180 - acosd(unit_vector(1)/cosd(declination(i)));
        end    
    end 
    
end
