%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Space Master 2018, Round 14                       %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
clc
close all

data = Data_problem_5;

% Common data for the three orbits
incl			= data.i;
right_asc_node 	= data.o;
arg_perige 		= data.w;
mu 				= data.G * data.Earth_mass;
n_periods		= 2;
n_periods_trans = 0.5;

% Data for LEO orbit
orbit_chosen    = data.LEO;
rp_LEO 		    = data.Radius_earth + data.height_LEO;
e_LEO 			= data.e_LEO;
a_LEO 			= rp_LEO / (1 - e_LEO);
Me_LEO			= data.Me(orbit_chosen);

[t_LEO, st_LEO, st0_LEO] = orbit_simulation_3D(e_LEO, a_LEO, Me_LEO, right_asc_node, incl, arg_perige, n_periods);


% Data for GEO orbit
orbit_chosen    = data.GEO;
e_GEO 			= data.e_GEO;
a_GEO			= (data.T_earth * sqrt(mu) / (2 * pi)) ^ (2/3);
Me_GEO 			= data.Me(orbit_chosen);

[t_GEO, st_GEO, st0_GEO] = orbit_simulation_3D(e_GEO, a_GEO, Me_GEO, right_asc_node, incl, arg_perige, n_periods);

% Data for the transfer orbit
orbit_chosen    = data.Transfer;
e_Transfer		= (a_GEO - rp_LEO) / (a_GEO + rp_LEO);
a_Transfer		= (rp_LEO + a_GEO) / 2;
Me_Transfer		= data.Me(orbit_chosen);

[t_Transfer, st_Transfer, st0_Transfer] = orbit_simulation_3D(e_Transfer, a_Transfer, Me_Transfer, right_asc_node, incl, arg_perige, n_periods_trans);

% a) Delta V 1 and 2
R_a = rp_LEO;
R_b = a_GEO;
R_c = R_a * (1 + e_LEO) / (1 - e_LEO);
R_d = R_b;

% Angular momentum for LEO
H1 = sqrt(R_a * mu * (1 + e_LEO));

% Angular momentum for the Transfer orbit
H2 = sqrt(R_a * mu * (1 + e_Transfer));

% Velocities at A for orbits LEO and Transfer
v_perigee_LEO = H1 / R_a; % Va1
v_perigee_Transfer = H2 / R_a; % Va2

% Velocities at B for orbits Transfer and GEO
v_apogee_Transfer = H2 / R_b; % Vb2
v_apogee_GEO = sqrt(mu / R_b); % Vb3, Circular orbit

% Delta V at A
delta_va = v_perigee_Transfer - v_perigee_LEO;
% Delta V at B
delta_vb = v_apogee_GEO - v_apogee_Transfer;

total_delta_v = delta_va + delta_vb;


fprintf("------ FROM PERIGEE ------\n");
fprintf("The velocities at the perigee for the LEO and the transfer orbit are " + ...
    v_perigee_LEO + " km/s and " + v_perigee_Transfer + " km/s\n");
fprintf("The velocities at the apogee for the transfer and the GEO orbit are " + ...
    v_apogee_Transfer + " km/s and " + v_apogee_GEO + " km/s\n");
fprintf("The first delta V is " + delta_va + " km/s and the second is " + ...
    delta_vb + " km/s\n");
fprintf("The total delta V is " + total_delta_v + " km/s\n\n");

% Velocities at C for orbits LEO and Transfer
v_apogee_LEO = H1 / R_c; % Va1
v_apogee_Transfer_2 = H2 / R_c; % Va2

% Velocities at D for orbits Transfer and GEO
v_perigee_Transfer_2 = H2 / R_d; % Vb2
v_perigee_GEO = sqrt(mu / R_d); % Vb3, Circular orbit

% Delta V at C
delta_vc = v_apogee_Transfer_2 - v_apogee_LEO;
% Delta V at D
delta_vd = v_perigee_GEO - v_perigee_Transfer_2;

total_delta_v_2 = delta_vc + delta_vd;

fprintf("------ FROM APOGEE ------\n");
fprintf("The velocities at the apogee for the LEO and the transfer orbit are " + ...
    v_apogee_LEO + " km/s and " + v_apogee_Transfer_2 + " km/s\n");
fprintf("The velocities at the perigee for the transfer and the GEO orbit are " + ...
    v_perigee_Transfer_2 + " km/s and " + v_perigee_GEO + " km/s\n");
fprintf("The first delta V is " + delta_vc + " km/s and the second is " + ...
    delta_vd + " km/s\n");
fprintf("The total delta V is " + total_delta_v_2 + " km/s\n\n");

% b) Plot the three orbits and their parameters

st = [st_LEO; st_Transfer; st_GEO];
jump1 = length(st_LEO);
jump2 = length(st_Transfer) + jump1;
jumps = [1; jump1; jump2; length(st)];
plot_multiple_satellites_data(st, jumps);

% c) Calculate the mass of the propelant
Isp = 300;          % seconds
mass_init = 2500;   % Kg
g = 0.00981;        % km/s2

fprintf("------ MASS PROPELANT ------\n");
mass_propelant_from_perigee = mass_init * (1 - exp(- total_delta_v / (Isp * g)));

fprintf("The propelant mass needed is " + mass_propelant_from_perigee + " Kg (from LEO's perigee)\n");

mass_propelant_from_apogee = mass_init * (1 - exp(- total_delta_v_2 / (Isp * g)));

fprintf("The propelant mass needed is " + mass_propelant_from_apogee + " Kg (from LEO's apogee)\n\n");

% d) Change in the inclination between two orbits during the transfer
% orbit.
% The most efficient points to do the inclination change are the perigee
% and the apogee of the tranfer orbit
total_delta_inclination = 10;

% Considering the total delta v starting from perigee
inclination_delta_v_LEO = 2 * v_perigee_LEO * sind(total_delta_inclination / 2);
total_delta_v_with_LEO_incl = total_delta_v + inclination_delta_v_LEO; 
inclination_delta_v_GEO = 2 * v_apogee_GEO * sind(total_delta_inclination / 2);
total_delta_v_with_GEO_incl = total_delta_v + inclination_delta_v_GEO;

fprintf("------ INCLINATION IN LEO and GEO  ------\n");
fprintf("The delta V needed for changing the plane on LEO perigee is " + inclination_delta_v_LEO + " Km/s\n");
fprintf("The total delta V needed for changing the plane on LEO perigee is " + total_delta_v_with_LEO_incl + " Km/s\n\n");

fprintf("The delta V needed for changing the plane on GEO is " + inclination_delta_v_GEO + " Km/s\n");
fprintf("The total delta V needed for changing the plane on GEO is " + total_delta_v_with_GEO_incl + " Km/s\n\n");

% Iteration to obtain the best delta_v to do the change of inclination
best_leo_inclination_change  = 0;
best_leo_delta_v_inclination = Inf(1);
best_geo_delta_v_inclination = Inf(1);

for delta_i = 0:0.05:10
    
    % Change in LEO
    incl_delta_v_LEO_iteration = sqrt(v_perigee_LEO^2 + v_perigee_Transfer^2 - ...
                                      2 * v_perigee_LEO * v_perigee_Transfer * cosd(delta_i));
    derivative_incl_delta_v_LEO_iteration = (v_perigee_LEO * v_perigee_Transfer * sind(delta_i)) / ...
                                            incl_delta_v_LEO_iteration;
    
    % Change in GEO
    geo_inclination_change = total_delta_inclination - delta_i;
    incl_delta_v_GEO_iteration = sqrt(v_apogee_Transfer^2 + v_apogee_GEO^2 - ...
                                      2 * v_apogee_Transfer * v_apogee_GEO * cosd(geo_inclination_change)); 
    derivative_incl_delta_v_GEO_iteration = (v_apogee_Transfer * v_apogee_GEO * sind(geo_inclination_change)) / ...
                                            incl_delta_v_GEO_iteration;
        
    derivative_total_inclination_delta_v_iteration = derivative_incl_delta_v_LEO_iteration - ...
        derivative_incl_delta_v_GEO_iteration;
    
    if abs(derivative_total_inclination_delta_v_iteration) < 0.1 && ...
            best_leo_delta_v_inclination > incl_delta_v_LEO_iteration && ...
            best_geo_delta_v_inclination > incl_delta_v_GEO_iteration
        best_leo_inclination_change  = delta_i;
        best_leo_delta_v_inclination = incl_delta_v_LEO_iteration;
        best_geo_delta_v_inclination = incl_delta_v_GEO_iteration;
    end
end


best_geo_inclination_change = total_delta_inclination - best_leo_inclination_change;
best_total_delta_v_inclination = best_leo_delta_v_inclination + best_geo_delta_v_inclination;
fprintf("------ BEST DELTA V FOUND FOR INCLINATION ------\n");
fprintf("The best angle found on LEO perigee is " + best_leo_inclination_change + " degrees\n");
fprintf("The best angle found on GEO is " + best_geo_inclination_change + " degrees\n\n");
fprintf("The delta V needed for changing the plane on LEO perigee is " + best_leo_delta_v_inclination + " Km/s\n");
fprintf("The delta V needed for changing the plane on GEO is " + best_geo_delta_v_inclination + " Km/s\n");
fprintf("The total delta V needed for changing the plane is " + best_total_delta_v_inclination + " Km/s\n\n");

mass_propelant_inclination = mass_init * (1 - exp(- (total_delta_v + best_total_delta_v_inclination) / (Isp * g)));

fprintf("The total propelant mass needed is " + mass_propelant_inclination + " Kg" + ...
        " for the change of orbit (delta_v = " + total_delta_v + " Km/s) and\n" + ...
        " the change of plane (delta_v = " + best_total_delta_v_inclination + " Km/s)\n");



% Plot the orbits with the plane change
[t_LEO_with_incl, st_LEO_with_incl, st0_LEO_with_incl] = orbit_simulation_3D(e_LEO, a_LEO, ...
                                                         Me_LEO, right_asc_node, ...
                                                         total_delta_inclination, arg_perige, n_periods);
transfer_orbit_incl = total_delta_inclination + best_leo_inclination_change;
[t_LEO_with_incl2, st_LEO_with_incl2, st0_LEO_with_incl2] = orbit_simulation_3D(e_LEO, a_LEO, ...
                                                         Me_LEO, right_asc_node, ...
                                                         transfer_orbit_incl, arg_perige, n_periods);
[t_Transfer_with_incl, st_Transfer_with_incl, st0_Transfer_with_incl] = orbit_simulation_3D(e_Transfer, ...
                                                         a_Transfer, Me_Transfer, right_asc_node, ...
                                                         transfer_orbit_incl, arg_perige, n_periods_trans);
[t_GEO_with_incl, st_GEO_with_incl, st0_GEO_with_incl] = orbit_simulation_3D(e_GEO, a_GEO, ...
                                                         Me_GEO, right_asc_node, transfer_orbit_incl, ...
                                                         arg_perige, n_periods);

st = [st_LEO_with_incl; st_LEO_with_incl2; st_Transfer_with_incl; st_GEO_with_incl; st_GEO];
jump1_with_incl = length(st_LEO_with_incl);
jump2_with_incl = length(st_LEO_with_incl2) + jump1_with_incl;
jump3_with_incl = length(st_Transfer_with_incl) + jump2_with_incl;
jump4_with_incl = length(st_GEO_with_incl) + jump3_with_incl;
jumps_with_incl = [1; jump1_with_incl; jump2_with_incl; jump3_with_incl; jump4_with_incl; length(st)];
plot_multiple_satellites_data(st, jumps_with_incl);

