%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef Data
    properties( Constant = true)
        % Orbit types
        LEO     = 1;
        GTO     = 2;
        GEO     = 3;
        Molniya = 4;
        Tundra  = 5;
        MEO     = 6;
        
        satellites_name = ["LEO", "GTO", "GEO", "Molniya", "Tundra", "MEO"];

        % Parameters for each orbit 41337.3
        a   = [  7978, 24400, 42241.0979,   26555,  42164, 26560];
        e   = [  0.05,  0.73,        0.2,    0.72,  0.075,  0.01];
        i   = [  47.2,     7,         10,    63.4,     43,  53.7];
        o   = [    30,    10,         30,     180,    195, 145.5];
        w   = [    45,     5,        325,     270,    270,  52.4];
        Me  = [    70,   250,        300,      90,    305, -37.3]; 

        % Ground stations
        Kiruna          = 1;
        Malindi         = 2;
        Cape_Canaveral  = 3;
        Tanegashima     = 4;
        
        ground_stations_name = ["Kiruna", "Malindi", "Cape_Canaveral", "Tanegashima"];
        
        % Latitude and longitude degrees, minutes and seconds
        degrees = 1;
        minutes = 2;
        seconds = 3;       
        
        % Latitude and Longitude for each ground station
        Latitude    = [[67,53,22]; [ 2,56,18]; [ 28, 31,26]; [ 30,23,60]]; % North, North, North, North
        Longitude   = [[20,13, 6]; [40,12,45]; [-80,-39,-3]; [130,58, 7]]; % Eest, Eest, Wast, Eest

        % Constants and Masses
        G               = 6.67408e-20;  % Km^3 * Kg^-1 * s^-2 
        Earth_mass      = 5.97219e24;   % Kg
        Radius_earth    = 6371;         % Km
        J2              = 1082 * 10^-6; % Oblateness gravity field coefficient 
        w_earth         = 15.04;        % degrees/hour
    end        
end