# orbit_determination

Orbit determination code from the subject Introduction to space mechanics and electronics, SpaceMasters Round 14. Code by Guillermo Zaragoza Prous, project done in collaboration with Ehssan Fiaz, Veronika Haberle and Mauro Rojas Sigala.

To run the code, use the script _orbits.m_. The data of the orbits is described in _Data.m_. There is specific code for the exercise 5, _orbits_problem_5.m_, where a change of orbit is needed. All the results are described in the file [Space_Mechanics_Report.pdf](https://gitlab.com/mrzaragoza/orbit_determination/blob/master/Space_Mechanics_Report.pdf).