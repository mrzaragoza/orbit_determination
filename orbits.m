%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Space Master 2018, Round 14                       %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
clc
close all

% Problem to solve [LEO, GTO, GEO, Molniya, Tundra, MEO]
% [Kiruna, Malindi, Cape_Canaveral, Tanegashima]
n_periods = 10;
data = Data;
ground_station      = data.Kiruna;
name_ground_station = data.ground_stations_name(ground_station);
    
for orbit_chosen = 1:6
    name_orbit      = data.satellites_name(orbit_chosen);
	e               = data.e(orbit_chosen);
	a               = data.a(orbit_chosen);
	Me              = data.Me(orbit_chosen);
	right_asc_node  = data.o(orbit_chosen);
	incl            = data.i(orbit_chosen);
	arg_perigee     = data.w(orbit_chosen);
    
    % Calculates the 3D orbit for the satellite
    [t, st, st0] = orbit_simulation_3D(e, a, Me, right_asc_node, incl, arg_perigee, n_periods);    
%     plot_satellite_data( st, st0, '3D', orbit_chosen);
    
    % Calculates the variables over time for the orbit of the satellite
    [r_magnitude, v_magnitude] = calculate_orbit_variables_over_time(st);
%     plot_orbit_variables_over_time(t, st, r_magnitude, v_magnitude, name_orbit);
    
    % Calculates the Ground Track for the orbit of the satellite
    [rigth_ascension, declination] = ground_track_calculation(t, st);
%     plot_ground_track(rigth_ascension, declination, name_orbit);
    
    % Calculates the Visibility of the orbit of the satellite for a given
    % Ground Station
    [elevation, azimuth] = orbit_visibility(st, rigth_ascension, declination, ground_station);
    plot_visibility(name_orbit, name_ground_station, elevation, azimuth);
    
%     plot_ground_track_plus_visibility(name_orbit, name_ground_station, ...
%                                            rigth_ascension, declination, ...
%                                            elevation, azimuth)
end
