%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Space Master 2018, Round 14                       %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_visibility(name_orbit, name_ground_station, elevation, azimuth)
    figure ('name', " Ground Station: " + name_ground_station + " Orbit: " + name_orbit);
    pax = polaraxes;
    azimuth_rad = deg2rad(azimuth);
    polarplot(azimuth_rad, elevation, '+');
    pax.ThetaZeroLocation = 'top';
    pax.ThetaDir          = 'clockwise';
    pax.RDir               = 'reverse';
    pax.RLim = [0 90];
end