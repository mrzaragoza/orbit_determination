%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Space Master 2018, Round 14                       %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_ground_track(rigth_ascension, declination, name_orbit)
    figure ('name', " Ground track: " + name_orbit);
    geoshow('landareas.shp', 'FaceColor', [1 1.0 1]);
    geoshow(declination, rigth_ascension);
end

