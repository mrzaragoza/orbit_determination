%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Space Master 2018, Round 14                       %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function r = iteration_over_true_anomaly(true_anom, h, mu, e)
% Iterates the true anomaly so we can see how the orbit looks like
	iteration_step  = 5;
	r               = zeros(2, 360/5);
	index           = 1;
	for i = true_anom : iteration_step : 360 + true_anom    
	    r_vector        = h^2 / ( mu * ( 1 + e * cosd(i) ) );
	    r( 1 , index)   = r_vector * cosd(i);
	    r( 2 , index)   = r_vector * sind(i);
	    index           = index + 1;
	end
end