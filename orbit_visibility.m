%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [elevation, azimuth] = orbit_visibility(st, right_ascension, declination, ground_station)

    elevation = [];
    azimuth   = [];

    data = Data();
    Radius_earth 	  = data.Radius_earth;
    latitude          = data.Latitude(ground_station, :);
    longitude         = data.Longitude(ground_station, :);
    latitude_degrees  = latitude(data.degrees) + latitude(data.minutes)/60 + ...
                        latitude(data.seconds) / 3600;
    longitude_degrees = longitude(data.degrees) + longitude(data.minutes)/60 + ...
                        longitude(data.seconds) / 3600;
%     right_ascension = right_ascension + 180;

    for index = 1:1:length(st)
       
        r_magnitude = norm(st(index,1:3));
        
        gamma = acosd(sind(declination(index)) * sind(latitude_degrees) + ...
                      cosd(declination(index)) * cosd(latitude_degrees) * ...
                      cosd(right_ascension(index) - longitude_degrees));
                  
        visibility_check = acosd(Radius_earth / r_magnitude); 
        if gamma <= visibility_check
            
            elevation_angle_denumerator = sqrt(1 + (Radius_earth / r_magnitude)^2 - ...
                  2 * (Radius_earth / r_magnitude) * cosd(gamma));
            elevation_angle = acosd( sind(gamma) / elevation_angle_denumerator);
            
            alpha = asind(sind(abs(longitude_degrees - right_ascension(index))) * ...
                cosd(declination(index)) / sind(gamma));
            
            azimuth_angle = azimuth_adjustment(alpha, declination(index),latitude_degrees, ...
                                               right_ascension(index),longitude_degrees);
            
            elevation = [elevation elevation_angle];
            azimuth = [azimuth azimuth_angle];
            
        end
    end
end

function azimuth = azimuth_adjustment(alpha, satellite_declination, groundstation_latitude, ...
                                      satellite_right_ascension, groundstation_longitude)
    
    azimuth = alpha;
    North_East = zeros(1,2);
    % Calculates if the satellite is East or West from the Ground Station
    if groundstation_longitude > 0 && ...
      (groundstation_longitude - 180) < satellite_right_ascension && ...
       satellite_right_ascension < groundstation_longitude
       North_East(2) = 0; 
    elseif groundstation_longitude > 0
       North_East(2) = 1;
    elseif groundstation_longitude < 0 && ...
       groundstation_longitude < satellite_right_ascension && ...
       satellite_right_ascension < (groundstation_longitude + 180)
       North_East(2) = 1;        
    elseif groundstation_longitude < 0
       North_East(2) = 0;
    end
       
    % Calculates if the satellite is North or South from the Ground Station
    if tand(groundstation_latitude) * ...
            cosd(satellite_right_ascension - groundstation_longitude) < ...
            tand(satellite_declination)
       North_East(1) = 1;
    else
       North_East(1) = 0; 
    end
    
    % Does adjustment depending on where the satellite is with respect to
    % the Ground Station
    
    if North_East(1) == 0 && North_East(2) == 0 % SAT is SW from GS
        azimuth = 180 + alpha;
    elseif North_East(1) == 0 && North_East(2) == 1 % SAT is SE from GS
        azimuth = 180 - alpha;
    elseif North_East(1) == 1 && North_East(2) == 0 % SAT is NW from GS
        azimuth = 360 - alpha;
    elseif North_East(1) == 1 && North_East(2) == 1 % SAT is NE from GS
        azimuth = alpha;
    end
    
end
