%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Space Master 2018, Round 14                       %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_orbit_variables_over_time(t, st, r_magnitude, v_magnitude, name_orbit)
	figure('name', name_orbit + ": Orbit variables over time")
	subplot(3,3,1);
	plot(t, st(:,1),'b-');
	title('x over time')

	subplot(3,3,2);
	plot(t, st(:,2),'b-');
	title('y over time')

	subplot(3,3,3);
	plot(t, st(:,3),'b-');
	title('z over time')

	subplot(3,3,4);
	plot(t, st(:,4),'b-');
	title('vx over time')

	subplot(3,3,5);
	plot(t, st(:,5),'b-');
	title('vy over time')

	subplot(3,3,6);
	plot(t, st(:,6),'b-');
	title('vz over time')
    
    subplot(3,3,7);
	plot(t, r_magnitude(:)','b-');
	title('r magnitude over time')
    
    subplot(3,3,8);
	plot(t, v_magnitude(:)','b-');
	title('v magnitude over time')    
end

