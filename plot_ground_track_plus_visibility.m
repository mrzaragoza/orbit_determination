%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Space Master 2018, Round 14                       %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_ground_track_plus_visibility(name_orbit, name_ground_station, ...
                                           rigth_ascension, declination, ...
                                           elevation, azimuth)
    figure ('name', " Ground Station: " + name_ground_station + " Orbit: " + name_orbit);
    subplot(1,2,1);
    geoshow('landareas.shp', 'FaceColor', [1 1.0 1]);
    geoshow(declination, rigth_ascension);
  
    pax = polaraxes;
    visibility_plot = subplot(1,2,2, pax);
    azimuth_rad = deg2rad(azimuth);
    polarplot(visibility_plot, azimuth_rad, elevation, '+');
    pax.ThetaZeroLocation = 'top';
    pax.ThetaDir          = 'clockwise';
    pax.RDir               = 'reverse';
    pax.RLim = [0 90];
end