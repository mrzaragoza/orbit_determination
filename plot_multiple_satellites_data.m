%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Space Master 2018, Round 14                       %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_multiple_satellites_data(st, jumps)

    % General variables
    figure('name', "Multiorbit plot");

	earth_sphere();
    hold on;
    for index = 2:1:length(jumps)   
		% Plots the result of the integration of the derivative of the state vector over time
		plot3(st(jumps(index - 1):jumps(index), 1), ...
              st(jumps(index - 1):jumps(index), 2), ...
              st(jumps(index - 1):jumps(index), 3), 'color', rand(1,3));
		hold on;
    end
    
    xlabel('X');
    ylabel('Y');
    zlabel('Z');
end
