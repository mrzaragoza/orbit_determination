%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Space Master 2018, Round 14                       %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function st_dot = satellite_orbit(t, st)
    global mu; 
    st_dot = zeros(6,1);
    
    % Copies the velocities from the state vector to its derivative
    st_dot(1) = st(4);
    st_dot(2) = st(5);
    st_dot(3) = st(6);
    
    % Obtains the magnitude of the position vector
    r_magnitude = sqrt( st(1)^2 + st(2)^2 + st(3)^2 );
    
    % Calculates the acceleration for the body
    st_dot(4) = - (mu / r_magnitude^3) * st(1);
    st_dot(5) = - (mu / r_magnitude^3) * st(2);
    st_dot(6) = - (mu / r_magnitude^3) * st(3);
end
