%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Space Master 2018, Round 14                       %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plot_satellite_data( st, st0, type, type_orbit)

    % General variables
	data = Data;
    figure('name', data.satellites_name(type_orbit));
    
    % Internal variables
    a   = data.a(type_orbit);
    e   = data.e(type_orbit);
    rp  = a * (1 - e);

	if type == '2D'
		% Plots the result of the state vector and the iteration of the true
		% anomaly
		%plot(r(1,:), r(2,:), 'r-.');
		%hold on;
		plot(st0(1), st0(2), 'g*');
		hold on;
		plot(st(:,1), st(:,2), 'b-.');
		hold on;
		plot_circunference_earth(type);
        line([-a+rp rp],[0 0]);
        xlabel('X');
        ylabel('Y');
		axis([-10000 10000 -10000 10000])
        
	elseif type == '3D'
		% Plots the result of the integration of the derivative of the state vector over time
		plot3(st0(1), st0(2), st0(3), 'g*');
		hold on;
		plot3(st(:,1), st(:,2), st(:,3), 'b-.');
		hold on;
		plot_circunference_earth(type);
        xlabel('X');
        ylabel('Y');
        zlabel('Z');
	end
end

function plot_circunference_earth(type)

	% Internal variables
	data = Data;
	radius_earth = data.Radius_earth;

	aux = 0 : pi / 30 : 2 * pi; 
	x   = (cos(aux) .* radius_earth);
	y   = sin(aux) .* radius_earth;
	plot(x, y, 'r');  
    if type == '3D'
        earth_sphere()
%         zero_array = zeros(length(x));
%         z   = (cos(aux) .* radius_earth);
%         plot3(zero_array, y, z, 'r'); 
    end
	plot(0, 0, 'r+');
end