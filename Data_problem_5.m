%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Introduction to Space Mechanics and Electronics   %%%
%%% Satellite Remote Sensing                          %%%
%%%                                                   %%%
%%% Ehssan Fiaz                                       %%%
%%% Veronika Haberle                                  %%%
%%% Mauro Rojas                                       %%%
%%% Guillermo Zaragoza Prous                          %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef Data_problem_5
    properties( Constant = true)
        % Orbit types
        LEO       = 1;
        Transfer  = 2;
        GEO       = 3;
        
        satellites_name = ["LEO", "Transfer", "GEO"];

        % Parameters 
        height_LEO    = 300; % Km
        e_LEO         = 0.001;
        e_GEO         = 0;
        i             = 0.01;
        o             = 10;
        w             = 270;
        Me            = [ 0, 0, 180]; 
        
        % Constants and Masses
        G               = 6.67408e-20;  % Km^3 * Kg^-1 * s^-2 
        Earth_mass      = 5.97219e24;   % Kg
        Radius_earth    = 6378;         % Km
        T_earth         = 86164;        % seconds
    end        
end